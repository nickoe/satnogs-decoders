"""
SatNOGS Processor subpackage initialization
"""
from __future__ import absolute_import, division, print_function

__all__ = ['ElfinPp']

from .elfin_pp import ElfinPp
